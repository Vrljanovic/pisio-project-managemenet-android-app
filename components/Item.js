import * as React from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, Text, Image, TouchableOpacity} from 'react-native';
import { images } from '../assets/images';
import Window from './Window';

class Item extends React.Component {
  constructor() {
    super();
    this.state = {
      isSelected: false,
      currentImage: images.expand
    }
  }

  onPress = () => {
    this.setState( {
      isSelected: !this.state.isSelected,
      currentImage: this.state.currentImage == images.expand? images.close : images.expand
    });
  }

    renderFinanceItem = (item) => {
    return(
      <View style={styles.detailView}>
        <Text style={styles.description}>Type: {item.payment_info.type}</Text>
        <Text style={styles.description}>Amount: {item.payment_info.amount} (BAM)</Text>
        <Text style={styles.description}>Payment Time: {item.payment_info.payment_time}</Text>
        <Text style={styles.description}>Project Name: {item.project_info.name}</Text>
      </View>
    );
  }

  renderAddButtonsForProject = () => {
    return (
      <View>
       <TouchableOpacity style={styles.buttonStyle} onPress={() => this.props.navigation.navigate('CreateProjectPart', {project_id: this.props.item.item.id, project_name: this.props.item.item.name})}>
          <Text style={styles.buttonText}>Add Project Part</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonStyle } onPress={() => this.props.navigation.navigate('CreateFinance', {project_id: this.props.item.item.id, project_name: this.props.item.item.name})}>
          <Text style={styles.buttonText}>Add Financial Entry</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderParticipantItem = (item) => {
    return(
      <View style={styles.detailView}>
        <Text style={styles.description}>Name: {item.participant_info.name}</Text>
        <Text style={styles.description}>Email: {item.participant_info.email}</Text>
        <Text style={styles.description}>Phone Number: {item.participant_info.phone_number}</Text>
        <Text style={styles.description}>Type: {item.participant_info.type}</Text>
        {item.extern_info != undefined && this.renderExternInfo(item)}
      </View>
    );
  }

  renderExternInfo = (item) => {
    return( 
      <View>
        <Text style={styles.description}>Project Part: {item.extern_info.project_part.job}</Text>
        <Text style={styles.description}>Role: {item.extern_info.role}</Text>
      </View>
    );
  }
  
   renderWorkerItem = (item) => {
    return(
      <View style={styles.detailView}>
        <Text style={styles.description}>Name: {item.name}</Text>
        <Text style={styles.description}>Email: {item.email}</Text>
        <Text style={styles.description}>Username: {item.username}</Text>
        <Text style={styles.description}>Job: {item.projectPart}</Text>
        <Text style={styles.description}>Project: {item.project}</Text>
      </View>
    );
  }

  renderProjectItem = (item) => {
    return(
      <View style={styles.detailView}>
        <Text style={styles.description}>Manager: {item.manager_name}</Text>
        <Text style={styles.description}>Manager Email: {item.manager_email}</Text>
        <Text style={styles.description}>Manager Username: {item.manager_username}</Text>
        <Text style={styles.description}>Client Name: {item.client_name}</Text>
        <Text style={styles.description}>Client Email: {item.client_email}</Text>
        <Text style={styles.description}>Client Phone Number: {item.client_phone}</Text>
        <Text style={styles.description}>Start Date: {item.start_date}</Text>
        <Text style={styles.description}>Deadline: {item.deadline}</Text>
        <Text style={styles.description}>Finished: {item.end_date == null? 'Not Finished' : item.end_date}</Text>
        <Text style={styles.description}>Project Parts:</Text>
        <View style={styles.indentView}>
        {item.project_parts.map((prop, key) => {
           return <Text style={styles.description}>{prop.job}</Text>;
        })}
        </View>
        <Text style={styles.description}>Financial Entries</Text>
        <View style={styles.indentView}>
        {item.finances.map((prop, key) => {
           return <Text style={styles.description}>{prop.description + ' (' + prop.amount + ' BAM)'}</Text>;
        })}
        </View>
        {this.props.loggedUser != undefined && this.props.loggedUser == item.manager_username && this.renderAddButtonsForProject()}
      </View>
    );
  }

  renderDetails = (item) => {
    if(this.props.itemType == 'Project')
      return this.renderProjectItem(item);
    else if (this.props.itemType == 'Worker')
      return this.renderWorkerItem(item);
    else if (this.props.itemType == 'Participant')
      return this.renderParticipantItem(item);
    else 
      return this.renderFinanceItem(item);
  }

 renderText(item){
   return(
   <Text style={styles.title}>{item.name == undefined && item.participant_info == undefined ? item.payment_info.description : (item.participant_info == undefined ?   item.name : item.participant_info.name)}</Text>
   );
 }
  render () {
    return(
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.onPress}>
          <View style={styles.titleContainer}>
            {this.renderText(this.props.item.item)}
            <Image source={this.state.currentImage} style={styles.image} />
          </View>
        </TouchableWithoutFeedback>
        {this.state.isSelected && this.renderDetails(this.props.item.item)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 10
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomWidth: 1,
  },
  title: {
    flex: 1,
    fontSize: 22
  },
  description: {
    flex: 1,
    fontSize: 18,
    color: 'gray',
    paddingTop: 10
  },
  image: {
    width: 20,
    height: 20
  },
  indentView : {
    marginLeft: 50
  },
  detailView: {
    backgroundColor: 'rgba( 0, 0, 0, 0.2 )'
  },
    buttonStyle: {
    marginTop: 10,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center'
  },
    buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  }
});

export { Item };