import * as React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import DatePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';

import { images } from '../assets/images';

export default class CreateFinanceScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      description: '',
      amount: 0,
      type: 'Expense',
      payment_time: '',
      project_id: 0,
    };
  }

   changeData = (key, value) => {
    if(this.state.project_id == 0)
      this.setState({project_id: this.props.navigation.state.params.project_id});
    if (key == 'description')
      this.setState({description: value});
    else if (key == 'amount')
      this.setState({amount: value});
    else if (key == 'type')
      this.setState({type: value});
    else 
      this.setState({payment_time: value});
  }

  handleAddPart = () => {
      if(this.state.description == ''  || this.state.payment_time == '' || this.state.type == '' || isNaN(this.state.amount) || this.state.amount <= 0){
      alert('Provide requested data');
      return;
    }
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restfinance/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      if (response.status == 201) {
        this.setState({
          description: '',
          amount: 0,
          type: 'Expense',
          payment_time: '',
          project_id: 0,
        });
        this.props.navigation.goBack();
      } else {
        alert('Error while trying to add financial entry. Try again later');
        this.props.navigation.goBack();
      }
    });
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        <Text
          style={{
            marginBottom: 30,
            fontWeight: 'bold',
            fontSize: 16,
            alignSelf: 'center',
          }}>
          Adding Financial Entry for{' '}
          {this.props.navigation.state.params.project_name}
        </Text>
        <Text style={styles.text}> Description: </Text>
        <TextInput
          style={styles.input}
          onChangeText={(value) => this.changeData('description', value)}
        />
        <Text style={styles.text}> Amount: </Text>
        <TextInput
          style={styles.input}
          onChangeText={(value) => this.changeData('amount', value)}
        />
        <Text style={styles.text}> Income/Expense: </Text>
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={[
            { label: 'Income', value: 'Income' },
            { label: 'Expense', value: 'Expense' },
          ]}
          onValueChange={(value, index) => this.changeData('type', value)}
        />
        <Text style={styles.text}> Payment Time: </Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('payment_time', value)}
          date={this.state.payment_time}
          mode="datetime"
          placeholder="Select Date"
          format="YYYY-MM-DD hh:mm:ss"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleAddPart}>
          <Text style={styles.buttonText}> Add Finance </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  text: {
    color: 'black',
    alignSelf: 'center',
    marginBottom: 2,
  },
  input: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  datePicker: {
    width: 200,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonStyle: {
    marginTop: 20,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '50%',
  },
});
