import * as React from 'react';
import {
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  Button,
} from 'react-native';

import { images } from '../assets/images';

export default class LoginForm extends React.Component {
  constructor() {
    super();
    this.state = {
      credentials: {
        username: '',
        password: '',
      }
    };
  }

  login = (event) => {
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restuser/login/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    username: this.state.username,
    password: this.state.password
  })
  }).then((response) => {
    if(response.status == 200){
      alert("Logged in");
      return this.props.navigation.navigate('Home', { loggedUser: this.state.username });
    }
    else
      alert('Wrong credentials');
  });
  }

  changeCredentials = (field, value) => {
    let old_cred =  this.state;
    if (field == 'username')
      old_cred.username = value;
    else
      old_cred.password = value;
    this.setState({credentials: old_cred});
  };

  

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        <Text style={styles.paragraph}>Please enter your credentials.</Text>
        <Text style={styles.text}>Username:</Text>
        <TextInput
          style={styles.field}
          autoCapitalize = 'none'
          onChangeText={(username) =>
            this.changeCredentials('username', username)
          }
        />
        <Text style={styles.text}>Password:</Text>
        <TextInput
          style={styles.field}
          secureTextEntry={true}
          autoCapitalize = 'none'
          onChangeText={(password) =>
            this.changeCredentials('password', password)
          }
        />
        <Button onPress={this.login} style={styles.loginButton} title="Login">
        </Button>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  field: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  loginButton: {
    height: 30,
    width: 200,
    alignSelf: 'center',
  },
  text: {
    alignSelf: 'center',
  },
});
