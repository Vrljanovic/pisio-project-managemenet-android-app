import * as React from 'react';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import { images } from '../assets/images';
import { Item } from './Item';

export default class Project extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      data: [],
    };
  }

  componentDidMount = () => {
    let url = '';
    if (this.props.navigation.state.params.screen == 'Project')
      url = 'http://pisio.etfbl.net/~nemanjav/vrlja/restproject/';
    else if (this.props.navigation.state.params.screen == 'Worker')
      url = 'http://pisio.etfbl.net/~nemanjav/vrlja/restworker/';
    else if (this.props.navigation.state.params.screen == 'Participant')
      url = 'http://pisio.etfbl.net/~nemanjav/vrlja/restparticipant/getall';
    else url = 'http://pisio.etfbl.net/~nemanjav/vrlja/restfinance/';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Error! Server returned code ' + response.status);
          return [];
        }
      })
      .then((response) => {
        this.setState({
          isLoading: false,
          data: response,
        });
      });
  };

  componentDidUpdate = () => {
    this.componentDidMount();
    return this.render();
  };

  renderItem = (item) => {
    return (
      <Item
        item={item}
        navigation={this.props.navigation}
        itemType={this.props.navigation.state.params.screen}
        loggedUser={this.props.navigation.state.params.loggedUser}
      />
    );
  };

  handleCreate = () => {
    if (this.props.navigation.state.params.screen == 'Project')
      this.props.navigation.navigate('CreateProject', {
        loggedUser: this.props.navigation.state.params.loggedUser,
      });
    else if (this.props.navigation.state.params.screen == 'Worker')
      this.props.navigation.navigate('CreateWorker', {
        loggedUser: this.props.navigation.state.params.loggedUser,
      });
    else if (this.props.navigation.state.params.screen == 'Participant')
      this.props.navigation.navigate('CreateParticipant', {
        loggedUser: this.props.navigation.state.params.loggedUser,
      });
  };
  renderCreateButton = () => {
    if (this.props.navigation.state.params.screen != 'Finance') {
      return (
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleCreate}>
          <Text style={styles.buttonText}>
            Create {this.props.navigation.state.params.screen}
          </Text>
        </TouchableOpacity>
      );
    }
  };
  addProjectPart(id) {
    this.props.navigation.navigate('CreateProjectPart', { project: id });
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text alignSelf="center">Loading</Text>
          <ActivityIndicator size="large" animating />
        </View>
      );
    }
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        {this.props.navigation.state.params.loggedUser != undefined &&
          this.renderCreateButton()}
        <ScrollView>
          <FlatList
            style={styles.listStyle}
            data={this.state.data}
            renderItem={(item) => this.renderItem(item)}
            keyExtractor={(index, _) => index + ''}
          />
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  listStyle: {
    marginTop: 20,
  },
  buttonStyle: {
    marginTop: 5,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
