import * as React from 'react';
import {
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import { createStackNavigator, StackNavigator } from 'react-navigation-stack';
import Constants from 'expo-constants';

import { images } from '../assets/images';

export default class MainMenu extends React.Component {
  static navigationOptions = {
    title: 'Main Menu',
  };
  
  renderMenu = () => {
    let loggedUser = this.props.navigation.state.params == undefined ? undefined : this.props.navigation.state.params.loggedUser;
    return (
          <View style={styles.container}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Projects', {screen: 'Project', loggedUser: loggedUser})}
              style={styles.buttonStyleFirst}>
              <Text style={styles.buttonText}>Projects</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Workers', {screen: 'Worker', loggedUser: loggedUser})}
              style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Workers</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Participants', {screen: 'Participant', loggedUser: loggedUser})}
              style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Participants</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('FinancialEntries', {screen: 'Finance', loggedUser: loggedUser})}
              style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Finance</Text>
            </TouchableOpacity>
          </View>
    );
  }
  render() {
    if (this.props.navigation.state.params == undefined || this.props.navigation.state.params.loggedUser == undefined) {
      return (
        <ImageBackground style={styles.backgroundImage} source={images.background}>
        <Text style={styles.welcomeMessage}>PISIO - Project Management</Text>
            {this.renderMenu()}
             <View style={styles.loginContainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Login')}
              style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      );
    }

    return (
      <ImageBackground style={styles.backgroundImage} source={images.background}>
        <Text style={styles.welcomeMessage}>PISIO - Project Management</Text>
        <Text style={styles.welcomeMessage}>Welcome {this.props.navigation.state.params.loggedUser}</Text>
        {this.renderMenu()}
        <View style={styles.loginContainer}>
            <TouchableOpacity
              onPress={() => {
                alert('Successfully logged out');
                this.props.navigation.navigate('Home', {loggedUser: undefined});
              }}
              style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    backgroundColor: 'white',
  },
  buttonStyleFirst: {
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },
  buttonStyle: {
    marginTop: 1,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  welcomeMessage: {
    marginTop: 70,
    alignSelf: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  loginContainer: {
    marginTop: 10,
    backgroundColor: '#87CEFA',
    position: 'absolute',
    alignSelf: 'flex-end',
    width: '100%',
    bottom: '10%'
  },
});
