import * as React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';

import { images } from '../assets/images';

export default class CreateWorkerScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      users: [],
      parts: [],
      user_id: 0,
      project_part_id: 0,
      role: '',
      time_spent: 0,
      loadingUsers: true,
      loadingProjects: true
    };
  }

  changeData = (key, value) => {
    if (key == 'user') this.setState({ user_id: value });
    else if (key == 'project') this.setState({ project_part_id: value });
    else if (key == 'role') this.setState({ role: value });
    else this.setState({ time_spent: value });
  };

  componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restuser/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Error! Server returned code ' + response.status);
          return [];
        }
      })
      .then((response) => {
        var users = [];
        let i = 0;
        for (let user of response) {
          users[i++] = { label: user.username, value: user.id };
        }
        this.setState({
          users: users,
          loadingUsers: false
        });
      });

      fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restprojectpart/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Error! Server returned code ' + response.status);
          return [];
        }
      })
      .then((response) => {
        var parts = [];
        let i = 0;
        for (let part of response) {
          parts[i++] = { label: part.job, value: part.id };
        }
        this.setState({
          projects: parts,
          loadingProjects: false
        });
      });
  };

  handleAddProject = () => {
    if(this.state.user_id == 0  || this.state.project_part_id == 0 || isNaN(this.state.time_spent) || this.state.time_spent <= 0 ){
      alert('Provide requested data');
      return;
    }
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restworker/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: this.state.user_id,
        project_part_id: this.state.project_part_id,
        role: this.state.role,
        time_spent: this.state.time_spent
      }),
    }).then((response) => {
      if (response.status == 200) {
        this.setState({
          users: [],
          projects: [],
          user: 0,
          project: 0,
          role: '',
          timeSpent: 0,
          loadingUsers: true,
          loadingProjects: true
        });
        this.props.navigation.goBack();
      } else if(response.status == 418){
        alert('That worker is already added for that part');
          this.setState({
          users: [],
          projects: [],
          user: 0,
          project: 0,
          role: '',
          timeSpent: 0,
          loadingUsers: true,
          loadingProjects: true
        });
        this.props.navigation.goBack();
      } 
      else {
        alert('Error while trying to add worker. Try again later');
        this.props.navigation.goBack();
      }
    });
   }

  render() {
    if (this.state.loadingUsers || this.state.loadingProjects) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text alignSelf="center">Loading</Text>
          <ActivityIndicator size="large" animating />
        </View>
      );
    }
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        <Text style={styles.text}>Worker: </Text>
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={this.state.users}
          onValueChange={(value, index) => this.changeData('user',  Number(value))}
        />
        <Text style={styles.text}>Project: </Text>
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={this.state.projects}
          onValueChange={(value, index) => this.changeData('project',  Number(value))}
        />
        <Text style={styles.text}>Role(optional): </Text>
        <TextInput
          style={styles.input}
          onChangeText={(value) => this.changeData('role', value)}
        />
        <Text style={styles.text}>Time spent (hours): </Text>
        <TextInput
          style={styles.input}
          onChangeText={(value) => this.changeData('timeSpent', Number(value))}
        />
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleAddProject}>
          <Text style={styles.buttonText}> Add Worker </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  text: {
    color: 'black',
    alignSelf: 'center',
    marginBottom: 2,
  },
  input: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonStyle: {
    marginTop: 20,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '50%',
  },
});
