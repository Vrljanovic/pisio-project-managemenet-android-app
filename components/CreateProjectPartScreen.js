import * as React from 'react';
import { ImageBackground, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { images } from '../assets/images';

export default class CreateProjectPartScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      job: '',
      man_hour: 0,
      percentage_done: 0,
      start_date: '',
      end_date: '',
      project_id: 0
    }
  }

  changeData = (key, value) => {
    if(this.state.project_id == 0)
      this.setState({project_id: this.props.navigation.state.params.project_id});
    if (key == 'job')
      this.setState({job: value});
    else if (key == 'man_hour')
      this.setState({man_hour: value});
    else if (key == 'percentage_done')
      this.setState({percentage_done: value});
    else if (key == 'start_date')
      this.setState({start_date: value});
    else 
      this.setState({end_date: value});
  }

  handleAddPart = () => {
     if(this.state.job == ''  || this.state.start_date == '' || this.state.end_date == '' || isNaN(this.state.man_hour) || this.state.man_hour <= 0 
     || isNaN(this.state.percentage_done) || this.state.percentage_done < 0){
      alert('Provide requested data');
      return;
    }
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restprojectpart/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      if (response.status == 201) {
        this.setState({
          job: '',
          man_hour: 0,
          percentage_done: 0,
          start_date: '',
          end_date: '',
          project_id: 0
        });
        this.props.navigation.goBack();
      } else {
        alert('Error while trying to add project part. Try again later');
        this.props.navigation.goBack();
      }
    });
   }

  render = () => {
    return(
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        <Text style={{marginBottom: 30, fontWeight: 'bold', fontSize: 16, alignSelf: 'center'}}>Adding Project Part for {this.props.navigation.state.params.project_name}</Text>
        <Text style={styles.text}> Job: </Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('job', value)}/>
        <Text style={styles.text}> Man hour: </Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('man_hour', value)} />
        <Text  style={styles.text}> Percentage Done: </Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('percentage_done', value)}/>
        <Text  style={styles.text}> Start Date: </Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('start_date', value)}
          date={this.state.start_date}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <Text  style={styles.text}> End Date: </Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('end_date', value)}
          date={this.state.end_date}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleAddPart}>
          <Text style={styles.buttonText}> Add Part </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  text: {
    color: 'black',
    alignSelf: 'center',
    marginBottom: 2,
  },
  input: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  datePicker: {
    width: 200,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonStyle: {
    marginTop: 20,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '50%',
  },
});
