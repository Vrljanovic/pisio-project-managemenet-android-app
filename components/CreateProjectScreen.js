import * as React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';

import { images } from '../assets/images';

export default class CreateProjectScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      clients: [],
      name: '',
      startDate: '',
      deadline: '',
      endDate: '',
      manager: '',
      client: 0,
      isLoading: 'true',
    };
  }

  changeData = (key, value) => {
    if (key == 'name') this.setState({ name: value });
    else if (key == 'startDate') this.setState({ startDate: value });
    else if (key == 'deadline') this.setState({ deadline: value });
    else if (key == 'endDate') this.setState({ endDate: value });
    else this.setState({ client: value });
  };

  componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restparticipant/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Error! Server returned code ' + response.status);
          return [];
        }
      })
      .then((response) => {
        var clients = [];
        let i = 0;
        for (let client of response) {
          clients[i++] = { label: client.name, value: client.id };
        }
        this.setState({
          clients: clients,
          isLoading: false,
          manager: this.props.navigation.state.params.loggedUser,
        });
      });
  };

  handleAddProject = () => {
    if(this.state.name == ''  || this.state.startDate == '' || this.state.deadline || this.state.client == 0){
      alert('Provide requested data');
      return;
    }
    fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restproject/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      if (response.status == 200) {
        this.setState({
          clients: [],
          name: '',
          startDate: '',
          deadline: '',
          endDate: '',
          manager: '',
          client: 0,
          isLoading: 'true',
        });
        this.props.navigation.goBack();
      } else {
        alert('Error while trying to add project. Try again later');
      }
    });
   }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text alignSelf="center">Loading</Text>
          <ActivityIndicator size="large" animating />
        </View>
      );
    }
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={images.background}>
        <Text style={styles.text}>Project Name: </Text>
        <TextInput
          style={styles.input}
          onChangeText={(value) => this.changeData('name', value)}
        />
        <Text style={styles.text}>Start Date: </Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('startDate', value)}
          date={this.state.startDate}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <Text style={styles.text}>Deadline: </Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('deadline', value)}
          date={this.state.deadline}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <Text style={styles.text}> End Date(If Project is Finished):</Text>
        <DatePicker
          style={styles.datePicker}
          onDateChange={(value) => this.changeData('endDate', value)}
          date={this.state.endDate}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
        />
        <Text style={styles.text}>Client: </Text>
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={this.state.clients}
          onValueChange={(value, index) => this.changeData('client', value)}
        />
        <Text
          style={{
            alignSelf: 'center',
            marginTop: 25,
            fontWeight: 'bold',
            fontSize: 18,
          }}>
          {' '}
          You are manager of this project!{' '}
        </Text>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleAddProject}>
          <Text style={styles.buttonText}> Add Project </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  text: {
    color: 'black',
    alignSelf: 'center',
    marginBottom: 2,
  },
  input: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  datePicker: {
    width: 200,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonStyle: {
    marginTop: 20,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '50%',
  },
});
