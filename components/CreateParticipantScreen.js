import * as React from 'react';
import { ImageBackground, StyleSheet, Text, TextInput, View, TouchableOpacity, ActivityIndicator, CheckBox } from 'react-native';
import PickerSelect from  'react-native-picker-select';

import { images } from '../assets/images';

export default class CreateParticipantScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      project_parts: [],
      name: '',
      email: '',
      phone_number: '',
      type: '',
      project_part_id: 0,
      role: '',
      isExtern: false,
      loadingParts: false,
      participant_id:0
    }
  }

  renderExternData = () => {
    return(
      <View>
        <Text style={styles.text}>Extern Participant on Part:</Text>
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={this.state.project_parts}
          onValueChange={(value, index) => this.changeData('project_part_id', value)}
        />
        <Text style={styles.text}> Role: </Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('role', value)} />
      </View>
    );
  }

  componentDidMount = () => {
     fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restprojectpart/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Error! Server returned code ' + response.status);
          return [];
        }
      })
      .then((response) => {
        var parts = [];
        let i = 0;
        for (let part of response) {
          parts[i++] = { label: part.job, value: part.id };
        }
        this.setState({
          project_parts: parts,
          loadingParts: false
        });
      });
  }

  changeData = (key, value) => {
      if(key == 'name')
        this.setState({name: value});
      else if (key == 'email')
        this.setState({email: value});
      else if (key == 'phone_number')
        this.setState({phone_number: value});
      else if(key == 'type')
        this.setState({type: value});
      else if (key == 'project_part_id')
        this.setState({project_part_id: value});
      else
        this.setState({role: value});
  }

  handleAddParticipant = () => {
    if(this.state.name =='' || this.state.email== '' || this.state.phone_number == '' || this.state.type== '' || 
      this.state.isExtern && (this.state.project_part_id == 0 || this.state.role == '')){
      alert('Please provide required data');
      return;
    } 
     fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restparticipant/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      } else {
        alert('Error while trying to add participant. Try again later');
        this.props.navigation.goBack();
        return ['Error']
      }
    }).then(response => {
      console.log(response)
      if(response != 'Error') {
        if(this.state.isExtern){
          this.setState({participant_id: response.id});
           this.addExternParticipant();
        }
      }
      this.props.navigation.goBack();
    });
  }

  addExternParticipant = () => {
      fetch('http://pisio.etfbl.net/~nemanjav/vrlja/restexternparticipant/create/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      } else {
        alert('Error while trying to add Extern participant');
        this.props.navigation.goBack();
        return ['Error']
      }
    });
  }

  render() {
    if(this.state.loadingParts){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text alignSelf="center">Loading</Text>
          <ActivityIndicator size="large" animating />
        </View>
      );
    }
    return(
      <ImageBackground style={styles.backgroundImage} source={images.background}>
        <Text style={styles.text}>Name:</Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('name', value)}/>
        <Text style={styles.text}>Email:</Text>
        <TextInput style={styles.input} autoCapitalize='none'  onChangeText={(value) => this.changeData('email', value)}/>
        <Text style={styles.text}>Phone Number:</Text>
        <TextInput style={styles.input} onChangeText={(value) => this.changeData('phone_number', value)} />
        <PickerSelect
          style={{
            inputAndroid: {
              width: 200,
              borderWidth: 1,
              alignSelf: 'center',
              fontColor: 'black',
              backgroundColor: '87CEFA',
            },
          }}
          items={[
            { label: 'Individual', value: 'Individual' },
            { label: 'Company', value: 'Company' },
          ]}
          onValueChange={(value, index) => this.changeData('type', value)}
        />
        <View style={styles.checkboxContainer}>
        <CheckBox
          value={this.state.isExtern}
          onValueChange={() => {
             this.setState({
                isExtern: !this.state.isExtern
             })
            }
          }
          style={styles.checkbox}
        />
        <Text style={styles.label}>Is Extern Participant</Text>
        </View>
        {this.state.isExtern && this.renderExternData()}
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={this.handleAddParticipant}>
          <Text style={styles.buttonText}> Add Participant </Text>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
   backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
    text: {
    color: 'black',
    alignSelf: 'center',
    marginBottom: 2,
  },
  input: {
    width: 200,
    height: 30,
    borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonStyle: {
    marginTop: 20,
    backgroundColor: '#87CEFA',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    width: '50%',
  },
  checkboxContainer: {
    alignSelf: 'center',
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});