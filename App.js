import * as React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MainMenu from './components/MainMenu';
import LoginForm from './components/LoginForm';
import Window from './components/Window';
import CreateProjectScreen from './components/CreateProjectScreen';
import CreateWorkerScreen from './components/CreateWorkerScreen';
import CreateProjectPartScreen from './components/CreateProjectPartScreen';
import CreateFinanceScreen from './components/CreateFinanceScreen';
import CreateParticipantScreen from './components/CreateParticipantScreen';

const MainNavigatorStack = createStackNavigator(
    {
      Home: {screen: MainMenu},
      Login: {screen: LoginForm},
      Projects: {screen: Window},
      Workers: {screen: Window},
      Participants: {screen: Window},
      FinancialEntries: {screen: Window},
      CreateProject: {screen: CreateProjectScreen},
      CreateWorker: {screen: CreateWorkerScreen},
      CreateProjectPart: {screen: CreateProjectPartScreen},
      CreateFinance: {screen: CreateFinanceScreen},
      CreateParticipant: {screen: CreateParticipantScreen}
    },
    {
      initialRouteName: "Home",
      defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#87CEFA',
      },
      headerTintColor: '#0060a0',
      headerTitleStyle: {
        fontWeight: 'bold',
      }
    },
    }
  );

const App = createAppContainer(MainNavigatorStack);
export default App;