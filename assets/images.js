export const images = {
  expand: require('./expand.png'),
  close: require('./close.png'),
  background: require('./background.jpg')
}